import requests
import random 
from PyDictionary import PyDictionary


def main():
	word = get_word()
	print(word)
	dictionary = PyDictionary()
	print (dictionary.meaning(word))

def get_word(): 
	_dict = get_dict()
	word_num = random.randint(0,len(_dict))
	return _dict[word_num].decode("ascii")

## Get dict of words.
def get_dict():
	word_site = "http://svnweb.freebsd.org/csrg/share/dict/words?view=co&content-type=text/plain"
	response = requests.get(word_site)
	return response.content.splitlines()


if __name__ == '__main__':
	main()